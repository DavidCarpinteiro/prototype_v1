package apdc.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.gson.Gson;

import apdc.exceptions.ExpiredTokenException;
import apdc.exceptions.InvalidAuthenticationToken;
import apdc.exceptions.UserDoesNotHavePermition;
import apdc.util.OutputStatusMessages;
import apdc.util.RegisterData;
import apdc.util.RequestResponse;
import apdc.util.TokenAuthenticator;
import apdc.util.UserInfo;

@Path("/action")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ActionResource {

	private static final Logger LOG = Logger.getLogger(UserResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public ActionResource() {

	}

	@GET
	@Path("/location")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getLocation(@HeaderParam("Authorization") String tokenID) {
		RequestResponse<String> response;

		try {
			Key userKey = TokenAuthenticator.authenticate(tokenID);
			Entity user = datastore.get(userKey);
			String tmp = (String) user.getProperty("user_zip_code");

			// TODO tmp until zipcode has a verification
			String zipcode = "2829";
			if (tmp != null && !tmp.isEmpty())
				zipcode = tmp;

			return Response.ok(g.toJson(new RequestResponse<String>(zipcode))).build();
		} catch (EntityNotFoundException | InvalidAuthenticationToken e) {
			LOG.warning("Invalid Token");
			response = new RequestResponse<String>(OutputStatusMessages.INVALIDE_TOKEN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		} catch (ExpiredTokenException e) {
			LOG.warning("Token has Expired");
			response = new RequestResponse<String>(OutputStatusMessages.EXPIRED_TOKEN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		}

	}

	@GET
	@Path("/list_users")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response listUsers(@HeaderParam("Authorization") String tokenID) {
		RequestResponse<String> response;

		try {
			Key userKey = TokenAuthenticator.authenticate(tokenID);
			List<String> out = new ArrayList<String>();

			Entity user = datastore.get(userKey);
			if (!user.getProperty("user_role").equals(RegisterData.ROLE_ADMIN))
				throw new UserDoesNotHavePermition();

			Query ctrQuery = new Query("User");// .setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

			for (Entity e : results) {
				out.add(e.getKey().getName());
			}
			if (out.isEmpty())
				out.add("No users");

			return Response.ok(g.toJson(new RequestResponse<String>(out))).build();

		} catch (UserDoesNotHavePermition e) {
			LOG.warning("User is not Admin");
			response = new RequestResponse<String>(OutputStatusMessages.NOT_ADMIN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		} catch (EntityNotFoundException | InvalidAuthenticationToken e) {
			LOG.warning("Invalid Token");
			response = new RequestResponse<String>(OutputStatusMessages.INVALIDE_TOKEN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		} catch (ExpiredTokenException e) {
			LOG.warning("Token has Expired");
			response = new RequestResponse<String>(OutputStatusMessages.EXPIRED_TOKEN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		}
	}

	@GET
	@Path("/users_info")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response usersInfo(@HeaderParam("Authorization") String tokenID) {
		RequestResponse<String> response;
		
		try {
			Key userKey = TokenAuthenticator.authenticate(tokenID);
			List<UserInfo> out = new ArrayList<UserInfo>();

			Entity user = datastore.get(userKey);
			if (!user.getProperty("user_role").equals(RegisterData.ROLE_ADMIN))
				throw new UserDoesNotHavePermition();

			Query ctrQuery = new Query("User");// .setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

			for (Entity e : results) {
				LOG.info(e.getKey().getName());
				UserInfo u = new UserInfo(e.getKey().getName(), (String) e.getProperty("user_role"),
						(String) e.getProperty("user_zip_code"));
				out.add(u);
			}
			LOG.warning(g.toJson(new RequestResponse<UserInfo>(out)));
			return Response.ok(g.toJson(new RequestResponse<UserInfo>(out))).build();

		} catch (UserDoesNotHavePermition e) {
			LOG.warning("User is not Admin");
			response = new RequestResponse<String>(OutputStatusMessages.NOT_ADMIN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		} catch (EntityNotFoundException | InvalidAuthenticationToken e) {
			LOG.warning("Invalid Token");
			response = new RequestResponse<String>(OutputStatusMessages.INVALIDE_TOKEN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		} catch (ExpiredTokenException e) {
			LOG.warning("Token has Expired");
			response = new RequestResponse<String>(OutputStatusMessages.EXPIRED_TOKEN);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(response)).build();
		}
	}

}
