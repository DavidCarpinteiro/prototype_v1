package apdc.resources;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

@Path("/cron")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class TokenRemoval {

	private static final Logger LOG = Logger.getLogger(UserResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public TokenRemoval() {

	}

	//TODO limit to admin only
	@POST
	@Path("/executeRemoval")
	public Response executeRemoval() {
		LOG.info("Token Removel Running");
		Query ctrQuery = new Query("Token");// TODO have table by date
		List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());

		if (results != null)
			for (Entity e : results) {
				if (System.currentTimeMillis() >= (long) e.getProperty("expiration_date")) {
					LOG.info("A Token Has Been Removed");
					datastore.delete(e.getKey());
				}

			}
		LOG.info("Token Removel Finished");
		return Response.ok().build();
	}

	@GET
	@Path("/tokenCleanUp")
	public Response removeTokens() {
		LOG.fine("Queueing Cleanup Service...");
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/rest/cron/executeRemoval"));
		return Response.ok().build();
	}

}
