package apdc.resources;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;

import apdc.exceptions.ExpiredTokenException;
import apdc.exceptions.InvalidAuthenticationToken;
import apdc.util.AuthToken;
import apdc.util.LoginData;
import apdc.util.OutputStatusMessages;
import apdc.util.PasswordChange;
import apdc.util.RegisterData;
import apdc.util.RequestResponse;
import apdc.util.TokenAuthenticator;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class UserResource {
	// Token TODO how to remove old tokens
	// TODO FUTURE: include cookie to check if is first time login, if not send
	// email confirmation

	private static final Logger LOG = Logger.getLogger(UserResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public UserResource() {
	}

	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response userRegister(RegisterData data) {
		LOG.info("Attempt to register user: " + data.username);

		if (!data.validRegistration()) {
			LOG.warning("Missing or Wrong Parameters");
			return Response.status(Status.BAD_REQUEST)
					.entity(new RequestResponse<String>(OutputStatusMessages.MISS_WRONG)).build();
		}

		Transaction txn = datastore.beginTransaction();
		try {
			Key userKey = KeyFactory.createKey("User", data.username);
			datastore.get(userKey);
			txn.rollback();

			LOG.warning("User already exists " + data.username);

			return Response.status(Status.BAD_REQUEST)
					.entity(g.toJson(new RequestResponse<String>(OutputStatusMessages.USER_EXISTS))).build();
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", data.username);
			user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
			user.setProperty("user_email", data.email);
			user.setProperty("user_role", data.role);
			user.setUnindexedProperty("user_main_name", data.name);
			user.setUnindexedProperty("user_adress", data.adress);
			user.setProperty("user_city", data.city);
			user.setProperty("user_zip_code", data.zipcode);
			user.setUnindexedProperty("user_creation_time", new Date());
			datastore.put(txn, user);

			LOG.info("User registered " + data.username);

			txn.commit();
			return Response.ok(g.toJson(new RequestResponse<String>(OutputStatusMessages.USER_REG))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}

	// Token and/or session id
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response userLogin(LoginData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {

		LOG.info("Attempt to login user: " + data.username);

		Transaction txn = null;
		Transaction tokenTran = null;// Created a new transaction for the token, cannot put entities with different
										// id
		// on database
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			txn = datastore.beginTransaction();
			Entity user = datastore.get(userKey);

			// Obtain the user login statistics
			Query ctrQuery = new Query("UserStats").setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity("UserStats", user.getKey());
				ustats.setProperty("user_stats_logins", 0L);
				ustats.setProperty("user_stats_failed", 0L);
			} else {
				ustats = results.get(0);
			}

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {

				// Construct the logs
				Entity log = new Entity("UserLog", user.getKey());
				log.setProperty("user_login_ip", request.getRemoteAddr());
				log.setProperty("user_login_host", request.getRemoteHost());
				log.setProperty("user_login_latlon", headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty("user_login_city", headers.getHeaderString("X-AppEngine-City"));
				log.setProperty("user_login_country", headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty("user_login_time", new Date());
				// Get the user statistics and updates it
				ustats.setProperty("user_stats_logins", 1L + (long) ustats.getProperty("user_stats_logins"));
				ustats.setProperty("user_stats_failed", 0L);
				ustats.setProperty("user_stats_last", new Date());

				// Batch operation
				List<Entity> logs = Arrays.asList(log, ustats);
				datastore.put(txn, logs);

				LOG.info("User '" + data.username + "' logged in sucessfully.");

				txn.commit();

				AuthToken token = new AuthToken(data.username);
				tokenTran = datastore.beginTransaction();

				Entity tokenEntity = new Entity("Token", token.tokenID);// , user.getKey()); //TODO explain
				tokenEntity.setProperty("username", user.getKey());
				tokenEntity.setProperty("expiration_date", token.expirationDate);
				tokenEntity.setProperty("creation_date", token.creationDate);
				datastore.put(tokenTran, tokenEntity);

				LOG.info("Token Added " + token.tokenID);

				// Failed Attempt at using cookies
				// Cookie c = new Cookie("tokenID", token.tokenID);
				// Date d = new Date();
				// d.setTime(token.expirationDate);
				// NewCookie cookie = new NewCookie(c, "comment",
				// (int)AuthToken.EXPIRATION_TIME, d, false, true);
				// NewCookie cookie = new NewCookie("tokenID", token.tokenID);
				// return Response.ok(g.toJson(token)).cookie(cookie).build();

				tokenTran.commit();
				return Response.ok(g.toJson(token)).build();
			} else {
				// Incorrect password
				ustats.setProperty("user_stats_failed", 1L + (long) ustats.getProperty("user_stats_failed"));
				datastore.put(txn, ustats);
				txn.commit();

				LOG.warning("Wrong password for username: " + data.username);

				return Response.status(Status.FORBIDDEN)
						.entity(new RequestResponse<String>(OutputStatusMessages.USER_PASS_INC)).build();
			}
		} catch (EntityNotFoundException e) {
			// username does not exist

			LOG.warning("Failed login attempt for username: " + data.username);

			return Response.status(Status.FORBIDDEN)
					.entity(new RequestResponse<String>(OutputStatusMessages.USER_PASS_INC)).build();
		} finally {
			boolean error = false;
			if (txn != null && txn.isActive()) {
				txn.rollback();
				error = true;
			}
			if (tokenTran != null && tokenTran.isActive()) {
				tokenTran.rollback();
				error = true;
			}
			if (error) {

				LOG.severe("Server ERROR");

				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new RequestResponse<String>(OutputStatusMessages.SERVER_ERROR)).build();
			}
		}

	}

	@DELETE
	@Path("/logout")
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	// @CookiePaream()
	public Response userLogout(@HeaderParam("Authorization") String tokenID) {
		LOG.info("Logout: " + tokenID);
		if (tokenID != null) {
			// NewCookie newCookie = new NewCookie(cookie, null, 0, false);
			// cookie.getValue();
			Key tokenKey = KeyFactory.createKey("Token", tokenID);
			datastore.delete(tokenKey);

			LOG.info("User Logout Successfully");

			return Response.ok(new RequestResponse<String>(OutputStatusMessages.LOGOUT_OK)).build();
		} else {
			LOG.warning("Token not present");
			return Response.status(Status.NO_CONTENT)
					.entity(new RequestResponse<String>(OutputStatusMessages.MISS_WRONG)).build();
		}
	}

	@PUT
	@Path("/password")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response userChangePassword(PasswordChange data) {
		if (!data.validData()) {
			return Response.status(Status.BAD_REQUEST)
					.entity(new RequestResponse<String>(OutputStatusMessages.MISS_WRONG)).build();
		}

		LOG.info("Attempt to change password user: " + data.username);

		Transaction txn = datastore.beginTransaction();
		try {
			Key userKey = KeyFactory.createKey("User", data.username);
			Entity user = datastore.get(userKey);

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password_old))) {
				user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password_new));
				datastore.put(txn, user);
				LOG.info("Password Changed for user " + data.username);
				txn.commit();

				return Response.ok(new RequestResponse<String>(OutputStatusMessages.PASS_CHANGE)).build();
			}

			else {

				LOG.warning("Wrong Password");

				return Response.status(Status.BAD_REQUEST)
						.entity(g.toJson(new RequestResponse<String>(OutputStatusMessages.USER_PASS_INC))).build();
			}
		} catch (EntityNotFoundException e) {
			txn.rollback();

			LOG.warning("User does not exist " + data.username);

			return Response.ok(g.toJson(new RequestResponse<String>(OutputStatusMessages.USER_PASS_INC))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();

				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(new RequestResponse<String>(OutputStatusMessages.SERVER_ERROR)).build();
			}
		}
	}

	// TODO working just on the server side
	@POST
	@Path("/refresh")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response userRefresh(@HeaderParam("Authorization") String tokenID) {
		Key tokenKey = KeyFactory.createKey("Token", tokenID);
		Transaction tokenTran = null;
		try {
			Key userKey = TokenAuthenticator.authenticate(tokenID);
			datastore.delete(tokenTran, tokenKey);
			
			AuthToken token = new AuthToken(userKey.getName());
			
			tokenTran = datastore.beginTransaction();
			
			Entity tokenEntity = new Entity("Token", token.tokenID);
			tokenEntity.setProperty("username", userKey);
			tokenEntity.setProperty("expiration_date", token.expirationDate);
			tokenEntity.setProperty("creation_date", token.creationDate);
			
			datastore.put(tokenTran, tokenEntity);
			
			LOG.info("Token Added " + token.tokenID);
			tokenTran.commit();
			
			
			
			LOG.info("Token Updated Successfully");
			return Response.ok(g.toJson(token) ).build();
		} catch (EntityNotFoundException | InvalidAuthenticationToken e) {
			LOG.warning("Token is not Valid");

			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(new RequestResponse<String>(OutputStatusMessages.INVALIDE_TOKEN)).build();
		} catch (ExpiredTokenException e) {
			LOG.warning("Token has Expired or Wrong Authentication Key");

			return Response.status(Status.FORBIDDEN)
					.entity(new RequestResponse<String>(OutputStatusMessages.EXPIRED_TOKEN)).build();
		} finally {
			if (tokenTran != null && tokenTran.isActive()) {
				tokenTran.rollback();
				LOG.severe("Server ERROR");
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(OutputStatusMessages.SERVER_ERROR).build();
			}

		}
	}

}
