package apdc.util;

import java.util.logging.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import apdc.exceptions.ExpiredTokenException;
import apdc.exceptions.InvalidAuthenticationToken;
import apdc.resources.UserResource;

public class TokenAuthenticator {

	private static final Logger LOG = Logger.getLogger(UserResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public TokenAuthenticator() {
	}

	public static Key authenticate(String tokenID)
			throws EntityNotFoundException, ExpiredTokenException, InvalidAuthenticationToken {
		LOG.info("Authenticating Token " + tokenID);

		// TODO better size
		if (tokenID == null || tokenID.length() == 0) {
			throw new InvalidAuthenticationToken();
		}

		Key tokenKey = KeyFactory.createKey("Token", tokenID);
		Entity tokenEntity = datastore.get(tokenKey);

		if (System.currentTimeMillis() <= (long) tokenEntity.getProperty("expiration_date")) {
			return (Key) tokenEntity.getProperty("username");
		}

		datastore.delete(tokenKey);
		throw new ExpiredTokenException();
	}

}
