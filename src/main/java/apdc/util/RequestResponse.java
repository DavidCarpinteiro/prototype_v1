package apdc.util;

import java.util.List;
import java.util.Map;

//Used for any kind of response to a REST request
public class RequestResponse<T> {
	//TODO JSONObject for output

	//Contains the type of Response Message
	public String type;

	// For a simple one String reply
	public String message;

	// For a list of a single type of data
	public List<T> messages;

	// For a list of complex types of data
	public Map<String, String> messagesMap;
	

	public RequestResponse() {
	}

	public RequestResponse(String message) {
		this.type = "single";
		this.message = message;
	}

	public RequestResponse(Map<String, String> messagesMap) {
		this.type = "list";
		this.messagesMap = messagesMap;
	}

	public RequestResponse(List<T> messages) {
		this.type = "map";
		this.messages = messages;
	}
	


}
