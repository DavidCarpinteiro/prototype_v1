package apdc.util;

//Used as input for the user registration data
public class PasswordChange {
	public String username;

	public String password_old;
	public String password_new;
	public String password_new_conf;

	public PasswordChange() {

	}

	public PasswordChange(String username, String password_old, String password_new, String password_new_conf) {
		this.username = username;
		this.password_new_conf = password_old;
		this.password_new = password_new;
		this.password_new_conf = password_new_conf;
	}

	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}

	public boolean validData() {
		return nonEmptyField(username) && nonEmptyField(password_old) && nonEmptyField(password_new)
				&& nonEmptyField(password_new_conf) && password_new.equals(password_new_conf) && PasswordValidator.validate(password_new);
	}
}
