package apdc.util;

import java.util.UUID;

//Creates a new Authentication Token
public class AuthToken {
	public static final long EXPIRATION_TIME = 1000 * 60 * 60 * 2; // 2h

	public String username;
	public String tokenID;
	public long creationDate;
	public long expirationDate;

	public AuthToken(String username) {
		this.username = username;
		this.tokenID = UUID.randomUUID().toString();
		this.creationDate = System.currentTimeMillis();
		this.expirationDate = this.creationDate + AuthToken.EXPIRATION_TIME;
	}

	public static long getExpirationData() {
		return System.currentTimeMillis() + AuthToken.EXPIRATION_TIME;
	}
}
