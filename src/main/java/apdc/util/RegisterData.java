package apdc.util;

//Used as input for the user registration data
public class RegisterData {
	public static final String ROLE_BASE = "u_basic";
	public static final String ROLE_WORKER = "u_worker";
	public static final String ROLE_ADMIN = "u_admin";

	// TODO better Administrator registration
	public static final String SECRET_CODE = "changeAdminRegistration";

	public String username;
	public String email;
	public String password;
	public String role;
	public String adress;
	public String city;
	public String zipcode;
	public String confirmation;
	public String name;
	public String secretCode;

	public RegisterData() {

	}

	public RegisterData(String username, String email, String password, String role, String adress, String city,
			String zipcode, String confirmation, String name, String secretCode) {
		this.username = username;
		this.password = password;
		this.confirmation = confirmation;
		this.name = name;
		this.email = email;
		this.role = role;
		this.adress = adress;
		this.city = city;
		this.zipcode = zipcode;
		this.secretCode = secretCode;
	}

	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}

	public boolean validRegistration() {
		boolean test;
		if (role.equals(ROLE_ADMIN))
			test = secretCode.equals(SECRET_CODE);
		else if(role.equals(ROLE_BASE) || role.equals(ROLE_WORKER))
			test = true;
		else
			test = false;

		return test && nonEmptyField(username) && nonEmptyField(password) && nonEmptyField(confirmation)
				&& nonEmptyField(email) && email.contains("@") && email.contains(".") && email.length() >= 5
				&& password.equals(confirmation) && PasswordValidator.validate(password);
	}
}
