package apdc.util;

public class OutputStatusMessages {
	
	public static final String SERVER_ERROR = "server_error";
	public static final String INVALIDE_TOKEN = "invalid_token";
	public static final String EXPIRED_TOKEN = "expired_token";
	public static final String LOGOUT_OK = "logout_ok";
	public static final String USER_PASS_INC = "username_password_incorrect";
	public static final String USER_REG = "user_registered";
	public static final String USER_EXISTS = "user_already_exists";
	public static final String MISS_WRONG = "missing_wrong_parameter";
	public static final String NO_OP = "no_operation_available";
	public static final String NOT_ADMIN = "user_not_admin";
	public static final String PASS_CHANGE = "password_changed";
	public static final String TOKEN_REF = "token_refreshed";
	
	
	public OutputStatusMessages() {
		
	}

}
