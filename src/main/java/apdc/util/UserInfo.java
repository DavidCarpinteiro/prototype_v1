package apdc.util;

public class UserInfo {
	
	public String name;
	public String role;
	public String location;
	
	public UserInfo() {
	}
	
	public UserInfo(String name, String role, String location) {
		this.name = name;
		this.role = role;
		this.location = location;
	}


}
