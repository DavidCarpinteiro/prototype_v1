package apdc.util;

public class PasswordValidator {

	public PasswordValidator() {

	}

	// TODO add this for username/email/zipcode
	// Code from https://stackoverflow.com/a/3802238/9026888
	public static boolean validate(String password) {
		// ^ # start-of-string
		// (?=.*[0-9]) # a digit must occur at least once
		// (?=.*[a-z]) # a lower case letter must occur at least once
		// (?=.*[A-Z]) # an upper case letter must occur at least once
		// (?=.*[@#$%^&+=]) # a special character must occur at least once
		// (?=\S+$) # no whitespace allowed in the entire string
		// .{8,} # anything, at least eight places though
		// $ # end-of-string
		//String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
		
		String pattern = "^(?=.*[0-9])(?=.*[a-z]).{4,}$";
		return (password.matches(pattern));
	}

}
