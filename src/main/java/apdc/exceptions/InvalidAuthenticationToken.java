package apdc.exceptions;

public class InvalidAuthenticationToken extends RuntimeException {
	static final long serialVersionUID = 0L;

	public InvalidAuthenticationToken() {
		super();
	}

	public InvalidAuthenticationToken(String message) {
		super(message);
	}

}
