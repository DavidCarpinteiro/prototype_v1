package apdc.exceptions;

public class ExpiredTokenException extends RuntimeException {
	static final long serialVersionUID = 0L;

	public ExpiredTokenException() {
		super();
	}

	public ExpiredTokenException(String message) {
		super(message);
	}

}
