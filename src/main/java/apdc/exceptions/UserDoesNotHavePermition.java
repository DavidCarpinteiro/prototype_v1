package apdc.exceptions;

public class UserDoesNotHavePermition extends RuntimeException {
	static final long serialVersionUID = 0L;

	public UserDoesNotHavePermition() {
		super();
	}

	public UserDoesNotHavePermition(String message) {
		super(message);
	}

}