window.onload = function () {
    $('#logout').click(doLogout);
}

doLogout = function (event) {
    if (localStorage.getItem("tokenID") == null) {
        alert("No user Login");
    } else {
        Jtoken = {
            type: "logout",
            tokenID: localStorage.getItem("tokenID")
        };
        console.log(JSON.stringify(Jtoken));
        $.ajax({
            type: "DELETE",
            url: "https://david-48092.appspot.com/rest/user/logout",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            headers: {
                "Authorization": localStorage.getItem('tokenID')
            },
            success: function (response) {
                if (response) {
                    if (response.message === "logout_ok") {
                        localStorage.removeItem('tokenID');
                        localStorage.removeItem('username');
                        localStorage.removeItem('expirationDate');
                        localStorage.removeItem('creationDate');
                        localStorage.removeItem("tokenID");
                        window.location.href = "/index.html";
                    }
                }
            },
            error: function (jqXHR, errorThrown, textStatus) {
                if ("missing_wrong_parameter" === jqXHR.responseJSON.message) {
                    alert("Missing or wrong parameter");
                }
            }
        });
    }


    event.preventDefault();

}
