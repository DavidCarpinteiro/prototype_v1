validadeValues = function (password) {
    // TODO check username/email, custom message depending on what is missing
    // Code from https://stackoverflow.com/a/3802238/9026888
    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

    return regex.test(password.value)
}
