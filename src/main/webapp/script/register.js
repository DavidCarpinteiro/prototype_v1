var form;

showNextPage = function (event) {
    var password = $('input[name="password"]');
    var confirm_password = $('input[name="confirmation"]');
    var val = validadeValues(password[0]);

    if (!val) {
        $('#pd').attr("placeholder", "1Upper, 1Lower, 1Digit, 1Special, 8min").val("").focus().blur();
    } else if (password[0].value != confirm_password[0].value) {
        $('#conf').attr("placeholder", "Passwords don't match!").val("").focus().blur();
    } else if (!form[0].checkValidity()) {
        alert("Please Fill In the Form Correctly");
    } else {
        $('div[id=area1]').hide();
        $('div[id=area2]').show();
    }
}

showBackPage = function (event) {
    $('div[id=area1]').show();
    $('div[id=area2]').hide();
}

captureData = function (event) {
    //TODO custom validity for zipcode/address/etc
    if (!form[0].checkValidity()) {
        alert("Please Fill In the Form Correctly");
    } else {
        data = form.jsonify();
        console.log(data);
        $.ajax({
            type: "POST",
            url: "https://david-48092.appspot.com/rest/user/register/",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,

            success: function (response) {
                if (response) {
                    localStorage.setItem('username', $('#user')[0].value);
                    window.location.href = "/pages/login.html";
                } else {
                    console.log("No response");
                }
            },
            error: function (jqXHR, errorThrown, textStatus) {
                if ("missing_wrong_parameter" === jqXHR.responseJSON.message) {
                    alert("Missing or wrong parameter");
                }
                if ("user_already_exists" === jqXHR.responseJSON.message) {
                    alert("User Already Exists");
                }
            },
            data: JSON.stringify(data)
        });
    }
    event.preventDefault();
};

window.onload = function () {
    form = $('form[name="register1"]');
    form[0].onsubmit = captureData;

    $("#nextbutton").click(showNextPage);
    $("#backbutton").click(showBackPage);
}
