var form;

captureData = function (event) {
    var password = $('input[name="password_new"]');
    var confirm_password = $('input[name="password_new_conf"]');
    var val = validadeValues(password[0]);

    if (!val) {
        $('#new_pass').attr("placeholder", "1Upper, 1Lower, 1Digit, 1Special, 8min").val("").focus().blur();
    } else if (password[0].value != confirm_password[0].value) {
        $('#new_pass_conf').attr("placeholder", "Passwords don't match!").val("").focus().blur();
    } else if (!form[0].checkValidity()) {
        alert("Please Fill In the Form Correctly");
    } else {
        var data = form.jsonify();
        $.ajax({
            type: "PUT",
            url: "https://david-48092.appspot.com/rest/user/password/",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            success: function (response) {
                if (response) {
                    localStorage.setItem('username', $("#user")[0].value)
                    localStorage.removeItem('tokenID');
                    localStorage.removeItem('expirationDate');
                    localStorage.removeItem('creationDate');
                    window.location.href = "/pages/login.html";
                } else {
                    console.log("No response");
                }
            },
            error: function (jqXHR, errorThrown, textStatus) {
                if ("username_password_incorrect" === jqXHR.responseJSON.message) {
                    alert("Username or Password are Incorrect");
                }
                if ("server_error" === jqXHR.responseJSON.message) {
                    alert("Please Try Again");
                }
            },
            data: JSON.stringify(data)
        });
    }
    event.preventDefault();
};

window.onload = function () {
    form = $('form[name="pass_change"]');
    $("#submit").click(captureData);
}
