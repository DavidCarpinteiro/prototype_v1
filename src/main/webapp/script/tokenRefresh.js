refresh = function () {
    console.log("Refreshing Token...");

    $.ajax({
        type: "POST",
        url: "https://david-48092.appspot.com/rest/user/refresh/",
        crossDomain: true,
        headers: {
            "Authorization": localStorage.getItem('tokenID')
        },
        success: function (response) {
            if (response) {
                console.log(response.tokenID);
                localStorage.setItem('tokenID', response.tokenID);
                localStorage.setItem('username', response.username);
                localStorage.setItem('expirationDate', response.expirationDate);
                localStorage.setItem('creationDate', response.expirationDate);
            } else {
                console.log("No response");
            }
        },
        error: function (jqXHR, errorThrown, textStatus) {
            if ("invalid_token" === jqXHR.responseJSON.message) {
                console.log("Invalid Token");
            }
            if ("expired_token" === jqXHR.responseJSON.message) {
                console.log("Expired Token");
            }
            if ("server_error" === jqXHR.responseJSON.message) {
                console.log("Please Try Again");
            }
        },
    });

    setTimeout(refresh, 1800000);
}

callRefresh = function () {
    if (localStorage.getItem("tokenID") == null) {
        console.log("No user Login");
    } else {
        refresh();
    }

}
