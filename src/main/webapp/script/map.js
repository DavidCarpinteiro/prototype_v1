var map;
var geocoder;
var usersLoc;
var coordinates = [];

function codeAddress(addr) {
    geocoder.geocode({
        address: addr
    }, function (results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                position: results[0].geometry.location,
                animation: google.maps.Animation.DROP,
                map: map
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function getLocation(addr, name, role) {
    geocoder.geocode({
        address: addr
    }, function (results, status) {
        if (status == 'OK') {
            var lat = results[0].geometry.location.lat();
            var lng = results[0].geometry.location.lng();

            var location = new google.maps.LatLng(lat, lng);
            var i;
            if (role === "u_basic")
                i = "https://cdn4.iconfinder.com/data/icons/Pretty_office_icon_part_2/48/man.png";
            else if (role === "u_worker")
                i = "https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Road-Worker-1-48.png";
            else if (role === "u_admin")
                i = "https://cdn3.iconfinder.com/data/icons/tango-icon-library/48/face-monkey-48.png";
            else
                i = "https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Hacker-48.png";

            var marker = new google.maps.Marker({
                position: location,
                map: map,
                icon: i,
            });

            var contentString = name + '<p>' + role;

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });

        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 38.659784,
            lng: -9.202765
        },
        zoom: 16
    });


    geocoder = new google.maps.Geocoder();
    showUsersLoc();
}

function showUsers() {
    for (i in usersLoc) {
        console.log(usersLoc[i].name);
        getLocation(usersLoc[i].location, usersLoc[i].name, usersLoc[i].role);


    }


}

window.onload = function () {
    loc = $("#showloc").click(showLocation);
    callRefresh();
}

showLocation = function (event) {
    if (localStorage.getItem("tokenID") == null) {
        alert("No user Login");
    } else {
        $.ajax({
            type: "GET",
            url: "https://david-48092.appspot.com/rest/action/location",
            crossDomain: true,
            headers: {
                "Authorization": localStorage.getItem('tokenID')
            },
            success: function (response) {
                if (response) {
                    codeAddress(response.message);
                }
            },
            error: function (jqXHR, errorThrown, textStatus) {
                if (jqXHR.responseJSON.message === "invalide_token")
                    alert("Please re-login");
                if (jqXHR.responseJSON.message === "expired_token")
                    alert("Please re-login");
            },
        });
    }
    event.preventDefault();
}

showUsersLoc = function () {
    if (localStorage.getItem("tokenID") == null) {
        alert("No user Login");
    } else {
        $.ajax({
            type: "GET",
            url: "https://david-48092.appspot.com/rest/action/users_info",
            crossDomain: true,
            headers: {
                "Authorization": localStorage.getItem('tokenID')
            },
            success: function (response) {
                if (response) {
                    usersLoc = response.messages;
                    showUsers();
                }
            },
            error: function (jqXHR, errorThrown, textStatus) {
                if (jqXHR.responseJSON.message === "invalide_token")
                    alert("Please re-login");
                if (jqXHR.responseJSON.message === "expired_token")
                    alert("Please re-login");
            },
        });
    }
}
