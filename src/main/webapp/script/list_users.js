listAllUsers = function () {
    var list = "";

    $.ajax({
        type: "GET",
        url: "https://david-48092.appspot.com/rest/action/list_users/",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        headers: {
            "Authorization": localStorage.getItem('tokenID')
        },
        success: function (response) {
            if (response) {
                list += "<table border='1'>"
                for (i in response.messages) {
                    console.log(response.messages[i]);
                    list += "<tr><td>" + response.messages[i] + "</td></tr>";
                }
                list += "</table>";
                $("#showList").html(list);
            } else {
                console.log("No response");
            }
        },
        error: function (jqXHR, errorThrown, textStatus) {
            //Currently not working
            if (jqXHR.status == 404 || errorThrown == 'Not Found') {
                console.log('There was a 404 error.');
            } else {
                var rp = jqXHR.responseJSON.message;
                if (rp === "user_not_admin")
                    alert("You need Admin Privileges");
                else if (rp === "invalide_token")
                    alert("Please re-login");
                else if (rp === "expired_token")
                    alert("Please re-login");
            }
        },

    });

}

window.onload = function () {
    $("#list").click(listAllUsers);
    callRefresh();
}
