var form;

captureData = function (event) {
    var data = form.jsonify();
    $.ajax({
        type: "POST",
        url: "https://david-48092.appspot.com/rest/user/login/",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        success: function (response) {
            if (response) {
                // Store in localStorage
                alert(response.tokenID);
                localStorage.setItem('tokenID', response.tokenID);
                localStorage.setItem('username', response.username);
                localStorage.setItem('expirationDate', response.expirationDate);
                localStorage.setItem('creationDate', response.expirationDate);
                window.location.href = "/pages/map.html";
            } else {
                console.log("No response");
            }
        },
        error: function (jqXHR, errorThrown, textStatus) {
            //console.log(jqXHR.status); capture 500 erros
            if ("username_password_incorrect" === jqXHR.responseJSON.message) {
                alert("Username or Password are Incorrect");
            }
            if ("server_error" === jqXHR.responseJSON.message) {
                alert("Please Try Again");
            }

        },
        data: JSON.stringify(data)
    });
    event.preventDefault();
};

window.onload = function () {
    form = $('form[name="login"]');
    form[0].onsubmit = captureData;
    var username = localStorage.getItem('username');
    if (username) {
        $("#user")[0].value = username;
    }
}

//TODO Refresh Token
/*
validateToken = function (event) {
    alert("on");

    d = new Date(); //1800000
    if (d.getTime() + 100000000 > localStorage.getItem('expirationData')) {
        Jtoken = {
            tokenID: localStorage.getItem("tokenID"),
            authKey: localStorage.getItem('authKey')
        };
        console.log(JSON.stringify(Jtoken));

        $.ajax({
            type: "PUT",
            url: "https://david-48092.appspot.com/rest/login/refresh",
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            success: function (response) {
                if (response) {
                    localStorage.setItem('tokenID', response.tokenID);
                    localStorage.setItem('username', response.username);
                    localStorage.setItem('expirationData', response.expirationData);
                } else {
                    alert("No response");
                }
            },
            error: function (response) {
                alert(response.responseText);
            },
            data: JSON.stringify(Jtoken)
        });


    }

}
*/
